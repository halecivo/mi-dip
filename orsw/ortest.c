#include <or1k-support.h>
#include <or1k-sprs.h>

#define INT_ADC 0x80
#define INT_SPI 0x40
#define INT_I2C 0x20

#define GPIO_BASE 0x91000000
#define SPI_BASE  0x97000000
#define I2C_BASE  0x98000000
#define ADC_BASE  0x92000000
#define NVM_BASE  0x95000000

#define RGPIO_IN GPIO_BASE
#define RGPIO_OUT (GPIO_BASE+0x4)
#define RGPIO_OE (GPIO_BASE+0x8)

#define SPI_SETUP (SPI_BASE+0x4)
#define SPI_STATUS (SPI_BASE+0x8)

#define I2C_DATA_IN (I2C_BASE)
#define I2C_DATA_OUT (I2C_BASE+0x38)
#define I2C_STATUS (I2C_BASE+0x20)
#define I2C_CONTROL (I2C_BASE+0x10)
#define I2C_TXN (I2C_BASE+0x18)

#define wb_write(addr, val) 	(*(unsigned int*) (addr) = (val))
#define wb_read(addr)		(*(unsigned int*) (addr))

#define OUT 1
#define IN 0

/*********************************
 * sensor register map definition
 * ******************************/

// INFO: 0000 0000
#define SREG_INFO 0x0
// CONTROL: 0000 00 conv meas
#define SREG_CONTROL 0x1
// STATUS: 0000 0000
#define SREG_DATA 0x2
#define SREG_STATUS 0x3
#define SREG_CONV 0x4
#define SREG_CONST 0x5
#define SREG_FREE1 0x6
#define SREG_SETUP 0x7
#define INV_SREG_FREE1 0x30

#define STATE_SPI_ADDR 0
#define STATE_SPI_DATA 1
#define SPI_WRITE 2

#define STATE_I2C_ADDR_MATCH 0
#define STATE_I2C_ADDR 1
#define STATE_I2C_DATA_MATCH 2
#define STATE_I2C_DATA 3

#define RO_FLAG(addr) ((ro_flags & (1 << addr)) > 0)

unsigned int ro_flags = 0x0000001C;
unsigned int spi_reg_addr = 0;
unsigned int i2c_reg_addr = 0;
unsigned int state_spi = 0;
unsigned int state_i2c = 0;

/**
 * set mode (IN/OUT) of gpio pin
 */
void gpio_mode (unsigned char pin, unsigned char mode){
	unsigned int gpio_modes = wb_read(GPIO_BASE + 0x8);
	
	if (mode == IN){
		gpio_modes &= ~(1 << pin);
	}
	else {
		gpio_modes |= (1 << pin);
	}

	wb_write(GPIO_BASE + 0x8, gpio_modes);
}

/**
 * set multiple gpio pin values
 */
void gpio_out(unsigned int values, unsigned int mask){
	unsigned int gpio_o_vals = wb_read(GPIO_BASE + 0x4);
	gpio_o_vals &= ~(mask);
	gpio_o_vals |= (values & mask);
	wb_write(RGPIO_OUT, gpio_o_vals);
}

/**
 * set value of gpio pin 
 */
void gpio_set(unsigned char pin, unsigned char value){
	value = (value > 1) ? 1 : value;
	gpio_out(value << pin, 1 << pin);
}


/**
 * write to i2c
 */
void i2c_write(unsigned int address, unsigned int data){
	*(unsigned int *)(I2C_BASE+(address << 2)) = data;
}

unsigned int i2c_read(unsigned int address){
	return *(unsigned int *)(I2C_BASE+(address << 2));
}

void nvm_write(unsigned int address, unsigned int data){
  wb_write(NVM_BASE+(address<<2), data);
}

unsigned int nvm_read(unsigned int address){
  return wb_read(NVM_BASE+(address<<2));
}

void reg_write(unsigned int address, unsigned int value){
  if (!RO_FLAG(address)){
    nvm_write(address, value);
    if (address == 7) {
      wb_write(SPI_SETUP, value);
    }
  } 
}

unsigned int reg_read(unsigned int address){
  if (address < 8) return nvm_read(address);
  else return 0xFF;
}

void i2c_handle_data_ready(){
  unsigned int bus_data = wb_read(I2C_STATUS); 
  if((bus_data & 0x04) == 0){
    bus_data = wb_read(I2C_DATA_IN);
    reg_write(i2c_reg_addr, bus_data);
  } else {
    wb_write(I2C_DATA_OUT, reg_read((i2c_reg_addr + 1) % 8));
  }
  state_i2c = STATE_I2C_ADDR_MATCH;
}

void handle_i2c_int(){
  unsigned int bus_data; 
  if(state_i2c == STATE_I2C_ADDR_MATCH){
    bus_data = wb_read(I2C_TXN);
    if(bus_data == 0x03){
      i2c_reg_addr = (i2c_reg_addr + 1) % 8;
      i2c_handle_data_ready();
    }
    else {
      state_i2c = STATE_I2C_ADDR;
    }
  }
  else if(state_i2c == STATE_I2C_DATA_MATCH){
    
    state_i2c = STATE_I2C_DATA;
  }
  else if(state_i2c == STATE_I2C_ADDR){
    bus_data = wb_read(I2C_DATA_IN);
    i2c_reg_addr = (bus_data >> 1);
    if (i2c_reg_addr > 7) i2c_reg_addr = 0; // wrap reg addr
    wb_write(I2C_DATA_OUT, reg_read(i2c_reg_addr)); 
    state_i2c = STATE_I2C_DATA_MATCH;
  }
  else {
    i2c_handle_data_ready();
  }
  wb_write(I2C_CONTROL, 0x43); // CLEAR I2C INTERRUPT
}

void handle_spi_int(){
  unsigned int bus_data;
  if (state_spi == STATE_SPI_ADDR){
    if (wb_read(SPI_STATUS) & 1){
      spi_reg_addr = (spi_reg_addr + 1) % 8;
      wb_write(SPI_BASE, reg_read(spi_reg_addr));
    }
    else {
      bus_data = *(unsigned int*)(SPI_BASE);
      spi_reg_addr = (bus_data >> 1);
      if (spi_reg_addr == 0x30){ // 0x30 (0x06 reversed) points to reg FREE1 to test LSB first mode
        spi_reg_addr = 0x06;
      }
      else if (spi_reg_addr > 7){
        spi_reg_addr = spi_reg_addr % 8;
      }       
      state_spi = ((bus_data & 1) << 1) | STATE_SPI_DATA;
      if (!(state_spi & SPI_WRITE)){
        *(unsigned int *)(SPI_BASE) = reg_read(spi_reg_addr);

      }
    }
  }
  else {
    if (state_spi & SPI_WRITE){
      reg_write(spi_reg_addr, *(unsigned int*)(SPI_BASE)); 
    } else {
      spi_reg_addr = (spi_reg_addr + 1) % 8;
      wb_write(SPI_BASE, reg_read(spi_reg_addr));
    }
    state_spi = STATE_SPI_ADDR;
  }
}

void handle_adc_int(){
  unsigned int meas_add = nvm_read(SREG_CONST) & 0x3F;
  unsigned int meas_shift = (nvm_read(SREG_CONST) & 0xC0) >> 6;
  nvm_write(SREG_DATA, wb_read(ADC_BASE));
  nvm_write(SREG_STATUS, 0x1);
  nvm_write(SREG_CONV, meas_add + (nvm_read(SREG_DATA) >> meas_shift));
}

int main (int argc, char *argv[]){
  unsigned int adc_data;
  unsigned long int int_val;
  
  int i;
  //or1k_exception_handler_add(0x00000200,adc_isr, 0x0);
  //or1k_interrupt_handler_add(6, adc_isr, 0x0);
  //or1k_interrupt_enable(6);

  nvm_write(SREG_INFO,      0x00);
  nvm_write(SREG_CONTROL,   0x00);
  nvm_write(SREG_DATA,      0x00);
  nvm_write(SREG_STATUS,    0x00);
  nvm_write(SREG_CONST,     0x00);
  nvm_write(SREG_CONV,      0xF0);
  nvm_write(SREG_FREE1,     0x00);
  nvm_write(SREG_SETUP,     0x00);

	gpio_set(0, 1);
	gpio_mode(0, OUT);
	gpio_mode(1, OUT);

	*(unsigned int *)(ADC_BASE+4) = 0x0000000F;

	// reset i2c core
	i2c_write(0x04,0x80); 
	i2c_write(0x04,0x00);
	i2c_write(0x04,0x40);
	// set prescaler
	i2c_write(0x02,0xFF);
	// set timeout
	i2c_write(0x0A,0xFF);
	// set device address
	i2c_write(0x0C,0x88);
	
  while(1){
    int_val = or1k_mfspr(OR1K_SPR_PIC_PICSR_ADDR);
    if(int_val != 0x0) {
      if(int_val & INT_ADC){ // ADC
        handle_adc_int();
      }
      if(int_val & INT_SPI){ // SPI
        handle_spi_int();
      }
      if(int_val & INT_I2C){ // I2C
        handle_i2c_int();
      }
    }
  }

	return 0;
}

