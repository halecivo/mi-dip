library IEEE;
use IEEE.STD_LOGIC_1164.all;

--library work;
entity wb_spi_slave is 
	port (
		-- wishbone interface
		wb_data_i, wb_add_i : in std_logic_vector (31 downto 0);   
		wb_we_i, wb_stb_i, wb_cyc_i, wb_clk_i, wb_rst_i : in std_logic;
 		irq, wb_ack_o : out std_logic;                 
    wb_data_o : out std_logic_vector (31 downto 0) := (OTHERS => '0');
		-- spi interface
		spi_ssel_i, spi_sck_i : in std_logic;
		spi_mosi_i : in std_logic;
		spi_miso_o : out std_logic);
end entity wb_spi_slave;


architecture wb_spi of wb_spi_slave is
component spi_slave is
    Generic (
        N : positive := 8;                                             -- 32bit serial word length is default^M
        PREFETCH : positive := 3);                                      -- prefetch lookahead cycles^M
    Port (  
        CPOL : std_logic := '0';                                        -- SPI mode selection (mode 0 default)^M
        CPHA : std_logic := '0';                                        -- CPOL = clock polarity, CPHA = clock phase.^M
        clk_i : in std_logic := 'X';                                    -- internal interface clock (clocks di/do registers)^M
        spi_ssel_i : in std_logic := 'X';                               -- spi bus slave select line^M
        spi_sck_i : in std_logic := 'X';                                -- spi bus sck clock (clocks the shift register core)^M
        spi_mosi_i : in std_logic := 'X';                               -- spi bus mosi input^M
        spi_miso_o : out std_logic := 'X';                              -- spi bus spi_miso_o output^M
        di_req_o : out std_logic;                                       -- preload lookahead data request line^M
        di_i : in  std_logic_vector (N-1 downto 0) := (others => 'X');  -- parallel load data in (clocked in on rising edge of clk_i)^M
        wren_i : in std_logic := 'X';                                   -- user data write enable^M
        wr_ack_o : out std_logic;                                       -- write acknowledge^M
        do_valid_o : out std_logic;                                     -- do_o data valid strobe, valid during one clk_i rising edge.^M
        do_o : out  std_logic_vector (N-1 downto 0);                    -- parallel output (clocked out on falling clk_i)^M
        --- debug ports: can be removed for the application circuit ---^M
        do_transfer_o : out std_logic;                                  -- debug: internal transfer driver^M
        wren_o : out std_logic;                                         -- debug: internal state of the wren_i pulse stretcher^M
        rx_bit_next_o : out std_logic;                                  -- debug: internal rx bit^M
        state_dbg_o : out std_logic_vector (3 downto 0);                -- debug: internal state register^M
        sh_reg_dbg_o : out std_logic_vector (N-1 downto 0)              -- debug: internal shift register^M
    );                      
end component spi_slave;

SIGNAL wr_ack_o : std_logic;
SIGNAL int_req : std_logic;
SIGNAL irq_sig : std_logic := '0';

SIGNAL data_reg : std_logic_vector (7 DOWNTO 0) := x"00";
SIGNAL setup_reg : std_logic_vector (7 DOWNTO 0) := x"00";
SIGNAL status_reg : std_logic_vector (7 DOWNTO 0) := x"00";
SIGNAL data_o_reg : std_logic_vector (7 DOWNTO 0) := x"00";

begin
spi : spi_slave port map (
        CPOL => setup_reg(0),
        CPHA => setup_reg(1),
        clk_i => wb_clk_i,
        spi_ssel_i => spi_ssel_i,
        spi_sck_i  => spi_sck_i,
        spi_mosi_i => spi_mosi_i,
        spi_miso_o => spi_miso_o,
        --di_req_o => irq,
        di_i => data_reg,
        wren_i => wb_we_i,
        wr_ack_o => wr_ack_o,
        do_valid_o => int_req,
        do_o   =>  data_o_reg

);

sreg : PROCESS (spi_ssel_i, int_req)
BEGIN
  IF spi_ssel_i = '1' THEN
    status_reg(0) <= '0';
    status_reg(1) <= '0';
  ELSIF int_req = '1' THEN
    status_reg(0) <= status_reg(1);
    status_reg(1) <= '1';
  ELSE 
    status_reg <= status_reg;
  END IF;
END PROCESS sreg;

wb_ack : PROCESS (wb_stb_i, wb_clk_i)
BEGIN
  IF rising_edge(wb_clk_i) THEN
    IF wb_stb_i = '1' THEN
      data_reg <= data_reg;
      setup_reg <= setup_reg;
      IF wb_add_i (9 DOWNTO 2) = x"00" and wb_we_i = '1' THEN
        data_reg <= wb_data_i (7 DOWNTO 0);
      ELSIF wb_add_i (9 DOWNTO 2) = x"00" and wb_we_i = '0' THEN
        wb_data_o (7 DOWNTO 0) <= data_o_reg;
      ELSIF wb_add_i (9 DOWNTO 2) = x"01" and wb_we_i = '1' THEN
        setup_reg <= wb_data_i (7 DOWNTO 0);
      ELSIF wb_add_i (9 DOWNTO 2) = x"02" and wb_we_i = '0' THEN
        wb_data_o (7 DOWNTO 0) <= status_reg;
      END IF;
      wb_ack_o <= '1' after 5 ns;
    ELSE
      wb_ack_o <= '0' after 5 ns;
    END IF;
  END IF;
END PROCESS wb_ack;

pic_irq : PROCESS (wb_stb_i, int_req)
BEGIN
  IF int_req = '1' THEN
    irq_sig <= '1';
  ELSIF wb_stb_i = '1' THEN
    irq_sig <= '0';
  ELSE
    irq_sig <= irq_sig;
  END IF;
END PROCESS pic_irq;

irq_out : PROCESS (wb_clk_i, irq_sig)
BEGIN
  IF rising_edge(wb_clk_i) THEN
    irq <= irq_sig;
  END IF;
END PROCESS irq_out;

end architecture wb_spi;

