library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity wb_adc is
  generic (
      ADC_RES : positive := 8
          );
  port (
    -- wishbone interface
    wb_data_i, wb_add_i : in std_logic_vector (31 downto 0);   
		wb_we_i, wb_stb_i, wb_cyc_i, wb_clk_i, wb_rst_i : in std_logic;
 		irq, wb_ack_o, wb_err_o : out std_logic;                 
    wb_data_o : out std_logic_vector (31 downto 0) := (OTHERS => '0');
    -- adc interface
    adc_data_i : in std_logic_vector (ADC_RES-1 DOWNTO 0);
    adc_start_o, adc_en_o, adc_clk_o : out std_logic;
    adc_ready_i : in std_logic);
end wb_adc;

architecture beh of wb_adc is
  -- 0x0 ADC_MEAS RO
  -- d7 d6 d5 d4 d3 d2 d1 d0 
  SIGNAL adc_measured : std_logic_vector (7 DOWNTO 0);
  -- 0x1 ADC_CNT RW
  -- 0 0 0 0  irq_en auto_start start en
  SIGNAL adc_control : std_logic_vector (7 DOWNTO 0);
  -- 0x2 ADC_ST RO
  -- 0 0 0 0  0 0 0 data_ready
  SIGNAL adc_status : std_logic_vector (7 DOWNTO 0); 

begin
  adc_meas : PROCESS (wb_clk_i, wb_rst_i, adc_ready_i)
  BEGIN
  IF rising_edge(wb_clk_i) THEN
    IF wb_rst_i = '1' THEN
      adc_measured <= (OTHERS => '0');
      adc_control <= (OTHERS => '0');
      adc_status <= (OTHERS => '0');
      irq <= '0';
      wb_err_o <= '0';
    ELSE
      adc_en_o <= adc_control(0);
      adc_start_o <= adc_control(1); -- set start SIGNAL to start value
      adc_control(1) <= adc_control(2); -- set start bit to autostart value
      IF adc_ready_i = '1' THEN
        adc_measured <= adc_data_i;
        irq <= adc_control(3);
      END IF;
      IF wb_stb_i = '1' THEN
        IF wb_add_i (9 DOWNTO 2) = x"00" and wb_we_i = '0' THEN
            wb_data_o (7 DOWNTO 0) <= adc_measured;
            irq <= '0';          
        END IF;
        IF wb_add_i (9 DOWNTO 2) = x"01" THEN
          IF wb_we_i = '1' THEN
            adc_control <= wb_data_i (7 DOWNTO 0);
          ELSE
            wb_data_o (7 DOWNTO 0) <= adc_control;
          END IF;
        END IF;
        IF wb_add_i (9 DOWNTO 2) = x"02" and wb_we_i = '0' THEN
            wb_data_o(7 DOWNTO 0) <= adc_status;         
        END IF;
      END IF;
    END IF;
  END IF;
  END PROCESS adc_meas;

  adc_clk_o <= wb_clk_i;

  wb_ack : PROCESS (wb_clk_i)
  BEGIN
    IF rising_edge(wb_clk_i) THEN
      IF wb_stb_i = '1' THEN
        wb_ack_o <= '1';
      ELSE
        wb_ack_o <= '0';
      END IF;
    END IF;
  END PROCESS wb_ack;

end architecture beh;
