library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.ALL;
use std.textio.all;

--library work;
entity wb_nvm is 
  generic(
    ADDR_W : positive := 7;
    DATA_W : positive := 8;
    MEM_FILE : string := "nvm.mem"
  );
	port (
		-- wishbone interface
		wb_add_i : in std_logic_vector (ADDR_W-1 downto 0);   
		wb_we_i, wb_stb_i, wb_cyc_i, wb_clk_i, wb_rst_i : in std_logic;
 		wb_ack_o : out std_logic;                 
    wb_data_i : in std_logic_vector (DATA_W-1 DOWNTO 0);
    wb_data_o : out std_logic_vector (DATA_W-1 downto 0) := (OTHERS => '0')
  );
end entity wb_nvm;


architecture nvm of wb_nvm is
  TYPE mem_type IS ARRAY (0 to (2**ADDR_W)-1) OF std_logic_vector (DATA_W-1 DOWNTO 0);
  SIGNAL mem : mem_type;

  impure function init_mem(mif_file_name : in string) return mem_type is
    file mif_file : text open read_mode is mif_file_name;
    variable mif_line : line;
    variable temp_bv : bit_vector(DATA_W-1 downto 0);
    variable temp_mem : mem_type;
  begin
    for i in mem_type'range loop
        readline(mif_file, mif_line);
        read(mif_line, temp_bv);
        temp_mem(i) := to_stdlogicvector(temp_bv);
    end loop;
    return temp_mem;
  end function;

  
  procedure save_mem(mem_file_name : in string; memory : in mem_type) is
    file mem_file : text open write_mode is mem_file_name;
    variable mem_line : line;
    variable temp_bv : bit_vector(DATA_W-1 downto 0);
    variable temp_mem : mem_type;
  begin
    for i in mem_type'range loop
        write(mem_line, to_bitvector(memory(i)));
        writeline(mem_file, mem_line);
    end loop;
  end procedure;

begin
wb_ack : PROCESS (wb_stb_i, wb_clk_i, wb_we_i, wb_rst_i)
BEGIN
  IF rising_edge(wb_clk_i) THEN
    IF wb_rst_i = '1' THEN
      mem <= init_mem(MEM_FILE);
    ELSIF wb_stb_i = '1' THEN
      IF wb_we_i = '1' THEN
        mem(to_integer(unsigned(wb_add_i))) <= wb_data_i;
        save_mem(MEM_FILE, mem);
      ELSE
        wb_data_o <= mem(to_integer(unsigned(wb_add_i)));
      END IF;
      wb_ack_o <= '1' after 5 ns;
    ELSE
      wb_ack_o <= '0' after 5 ns;
    END IF;
  END IF;
END PROCESS wb_ack;

end architecture nvm;

