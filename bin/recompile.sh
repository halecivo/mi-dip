#!/bin/bash
or1k-elf-gcc -mboard=orpsocrefdesign -mcompat-delay ../orsw/ortest.c -o ../orsw/ortest
or1k-elf-objcopy -O binary ../orsw/ortest ../orsw/ortest.bin
./bin2hex ../orsw/ortest.bin 4 -size_word > ../orsw/ortest.hex
sed -i "s/+firmware_size=[0-9]*/+firmware_size=$(cat ../orsw/ortest.hex | wc -l)/g" ../inc/sensor_tb.f
