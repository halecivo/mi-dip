class sensor_env extends uvm_env;
  `uvm_component_utils(sensor_env)

  // data members
  spi_agent m_spi_agent;
  i2c_agent m_i2c_agent;
  adc_agent m_adc_agent;
  sensor_env_config m_cfg;

  sensor_virtual_sequencer m_v_sqr;
  sensor_scoreboard m_scoreboard;

  // Constraints

  // Methods

  extern function new(string name = "sensor_env", uvm_component parent = null);
  extern function void build_phase(uvm_phase phase);
  extern function void connect_phase(uvm_phase phase);

endclass: sensor_env

function sensor_env::new(string name = "sensor_env", uvm_component parent = null);
  super.new(name, parent);
endfunction

function void sensor_env::build_phase(uvm_phase phase);
  if(!uvm_config_db #(sensor_env_config)::get(this, "", "sensor_env_config", m_cfg)) begin
    `uvm_error("build_phase", "unable to get sensor_env_config");
  end
  if(m_cfg.has_spi_agent) begin
    uvm_config_db #(spi_agent_config)::set(this, "m_spi_agent*", "spi_agent_config", m_cfg.m_spi_agent_cfg);
    m_spi_agent = spi_agent::type_id::create("m_spi_agent", this);
  end
  if(m_cfg.has_i2c_agent) begin
    uvm_config_db #(i2c_agent_config)::set(this, "m_i2c_agent*", "i2c_agent_config", m_cfg.m_i2c_agent_cfg);
    m_i2c_agent = i2c_agent::type_id::create("m_i2c_agent", this);
  end
  if(m_cfg.has_adc_agent) begin
    uvm_config_db #(adc_agent_config)::set(this, "m_adc_agent*", "adc_agent_config", m_cfg.m_adc_agent_cfg);
    m_adc_agent = adc_agent::type_id::create("m_adc_agent", this);
  end
  if (m_cfg.has_virtual_sequencer) begin
    m_v_sqr = sensor_virtual_sequencer::type_id::create("m_v_sqr", this);
  end
  if (m_cfg.has_scoreboard) begin
    m_scoreboard = sensor_scoreboard::type_id::create("m_scoreboard", this);
    m_scoreboard.m_cfg = m_cfg;
  end

endfunction: build_phase

function void sensor_env::connect_phase(uvm_phase phase);
  if(m_cfg.has_virtual_sequencer) begin
    if(m_cfg.has_spi_agent) begin
      m_v_sqr.spi = m_spi_agent.m_sequencer;
    end
    if(m_cfg.has_i2c_agent) begin
      m_v_sqr.i2c = m_i2c_agent.m_sequencer;
    end
  end
  if (m_cfg.has_scoreboard) begin
    if (m_cfg.has_spi_agent) m_spi_agent.ap.connect(m_scoreboard.spi.analysis_export);
    if (m_cfg.has_i2c_agent) m_i2c_agent.ap.connect(m_scoreboard.i2c.analysis_export);
    if (m_cfg.has_adc_agent) m_adc_agent.ap.connect(m_scoreboard.adc.analysis_export);
  end
endfunction: connect_phase
