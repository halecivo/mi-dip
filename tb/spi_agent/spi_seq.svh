class spi_seq extends uvm_sequence #(spi_seq_item);

  `uvm_object_utils(spi_seq)

  extern function new(string name = "spi_seq");
  extern task body;
  int num_txn = 10;

endclass: spi_seq


function spi_seq::new(string name = "spi_seq");
  super.new(name);
endfunction

task spi_seq::body;
  spi_seq_item req;

  begin
    for (int i = 0; i < num_txn; i++) begin
      req = spi_seq_item::type_id::create("request");
      start_item(req);
      assert(req.randomize() with {
        addr != 7'h07;
        write == 1;
        item_length == 16;
      });
      finish_item(req);
      start_item(req);
      req.write = 0;
      finish_item(req);
    end
  end

endtask: body


