// Class Description:

class spi_agent_config extends uvm_object;

  localparam string s_my_config_id = "spi_agent_config";
  localparam string s_no_config_id = "no config";
  localparam string s_my_config_type_error_id = "config type error";

  // UVM Factory Registration Macro
  `uvm_object_utils(spi_agent_config)

  // Virtual Interface 
  virtual spi_if SPI;

  // Data Members

  // Is the agent active or pasive
  uvm_active_passive_enum active = UVM_ACTIVE;
  // Include the SPI functional coverage monitor
  bit has_functional_coverage = 1;

  // SPI configuration
  // clock polarity
  bit cpol = 0;
  // clock phase
  bit cphase = 0;
  time sck_period = 35us;
  time bus_free = 100us; // bus free time between two transactions
  bit MSB = 1; // 1 = MSB first

  // timing config
  time THold_csb   = 5000ns;
  time TSetup_csb  = 5000ns;
  time TSetup_mosi = 20ns;
  time THold_mosi  = 20ns;


  // checks config
  bit checks_enable = 0;
  bit timing_checks_enable = 1;
  time TSetup_miso = 5ns;
  time THold_miso = 5ns;  

  // Methods
  extern static function spi_agent_config get_config(uvm_component c);
  extern function new(string name = "spi_agent_config");

endclass: spi_agent_config

  function spi_agent_config::new(string name = "spi_agent_config");
    super.new(name);
  endfunction

  function spi_agent_config spi_agent_config::get_config(uvm_component c);
    spi_agent_config t;
    if(!uvm_config_db #(spi_agent_config)::get(c, "", s_my_config_id, t)) begin
    `uvm_fatal("CONFIG_LOAD", $sformatf("Cannot get() configuration %s from uvm_config_db. Have you set() it?", s_my_config_id))
  end

    return t;
  endfunction: get_config
