class spi_sequencer extends uvm_sequencer #(bus_seq_item, bus_seq_item);

  `uvm_component_utils(spi_sequencer)

  extern function new(string name="spi_sequencer", uvm_component parent = null);

endclass: spi_sequencer

function spi_sequencer::new(string name="spi_sequencer", uvm_component parent = null);
  super.new(name, parent);
endfunction
