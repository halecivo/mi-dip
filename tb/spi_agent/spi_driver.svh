class spi_driver extends uvm_driver #(bus_seq_item);
  `uvm_component_utils(spi_driver)

  `define IS_FIRST_BYTE i == 0
  `define IS_LAST_BYTE i == (byte_num - 1)

  bus_seq_item item;
  spi_seq_item req;
  spi_agent_config m_cfg;

  virtual spi_if SPI;

  function new (string name = "spi_driver", uvm_component parent = null);
    super.new(name, parent);
  endfunction

  virtual function void build_phase (uvm_phase phase);
    super.build_phase(phase);
    if (m_cfg == null) begin
        if(!uvm_config_db #(spi_agent_config)::get(this, "", "spi_agent_config", m_cfg)) begin
          `uvm_fatal("CONFIG_LOAD", $sformatf("Cannot get() configuration %s from uvm_config_db. Have you set() it?", "spi_agent_config"))
        end
    end
  endfunction: build_phase

  task reset_phase (uvm_phase phase);
    //spi_seq_item req = spi_seq_item::type_id::create("req");
    //  `uvm_info("spi driver: ", "reset phase", UVM_INFO);
    //SPI.mosi = 'bz;
    //SPI.miso = 'bz;
    //SPI.sck = m_cfg.cpol;
    //SPI.csb = 1;
    //req.write = 0;
    //req.push_byte(8'h03);
    //drive_txn(req);
  endtask: reset_phase
    

  task run_phase (uvm_phase phase);

    // Default conditions:
    SPI.mosi = 'bz;
    SPI.miso = 'bz;
    SPI.sck = m_cfg.cpol;
    SPI.csb = 1;
    req = spi_seq_item::type_id::create("spi_seq_item");

    forever begin
      seq_item_port.get_next_item(item);
      req.do_copy(item);
      if(m_cfg.MSB == 0) req.reverse();
      req.serialize();
      `uvm_info("SPI DRIVER: ", req.to_string(), UVM_INFO)
      #(m_cfg.bus_free/2);
      if (SPI.sck != m_cfg.cpol) begin
        SPI.sck = m_cfg.cpol;
        `uvm_info("SPI DRIVER: ", "Change of CPOL detected, switching scl", UVM_INFO);
      end
      #(m_cfg.bus_free/2);
      drive_txn(req);
      seq_item_port.item_done();
    end

  endtask: run_phase


  task wait_active_sample_edge();
    if(m_cfg.cpol != m_cfg.cphase) @(negedge SPI.sck);
    else @(posedge SPI.sck);
  endtask

  task wait_active_drive_edge();
    if(m_cfg.cpol == m_cfg.cphase) @(negedge SPI.sck);
    else @(posedge SPI.sck);
  endtask

  task sck_generate();
    forever begin
      SPI.sck <= ~SPI.sck;
      #(m_cfg.sck_period/2);
    end
  endtask;

  task drive_first_bit(bit b);
      if (m_cfg.TSetup_mosi > 0) SPI.mosi <= 'bx; // put X on mosi only if TSetup_mosi is set
      else SPI.mosi <= b; 
      if (m_cfg.cphase == 1) #(m_cfg.sck_period/2 - m_cfg.TSetup_mosi); 
      else #(m_cfg.THold_csb - m_cfg.TSetup_mosi); // alternative waiting before first bit of txn if cphase == 0
      SPI.mosi <= b;
      #(m_cfg.TSetup_mosi + m_cfg.THold_mosi);
      if (m_cfg.THold_mosi > 0) SPI.mosi <= 'bx; // put X on mosi only if THold_mosi is set
      #(m_cfg.sck_period/2 - m_cfg.THold_mosi); // final waiting only if not last bit of txn or cphase == 0
  endtask

  task drive_bit(bit b);
      if (m_cfg.TSetup_mosi > 0) SPI.mosi <= 'bx; // put X on mosi only if TSetup_mosi is set
      else SPI.mosi <= b; 
      #(m_cfg.sck_period/2 - m_cfg.TSetup_mosi); 
      SPI.mosi <= b;
      #(m_cfg.TSetup_mosi + m_cfg.THold_mosi);
      if (m_cfg.THold_mosi > 0) SPI.mosi <= 'bx; // put X on mosi only if THold_mosi is set
      #(m_cfg.sck_period/2 - m_cfg.THold_mosi); // final waiting only if not last bit of txn or cphase == 0
   endtask
         
  task drive_last_bit(bit b);
      if (m_cfg.TSetup_mosi > 0) SPI.mosi <= 'bx; // put X on mosi only if TSetup_mosi is set
      else SPI.mosi <= b; 
      #(m_cfg.sck_period/2 - m_cfg.TSetup_mosi); 
      SPI.mosi <= b;
      #(m_cfg.TSetup_mosi + m_cfg.THold_mosi);
      if (m_cfg.THold_mosi > 0) SPI.mosi <= 'bx; // put X on mosi only if THold_mosi is set
      if (m_cfg.cphase == 0) #(m_cfg.sck_period/2 - m_cfg.THold_mosi); // final waiting only if not last bit of txn or cphase == 0
  endtask   


  task drive_txn(spi_seq_item req);
      int byte_num = req.get_data_bytes_cnt();
      int last_byte_bits = req.item_len % 8;
      if (last_byte_bits == 0) last_byte_bits = 8;
      SPI.csb <= 0;
      fork
        begin
          if (m_cfg.cphase == 1) begin
            #m_cfg.THold_csb;
          end
          drive_first_bit(req.get_bit_mosi(0));
          for (int i = 1; i < (req.item_len-1); i++) begin
            drive_bit(req.get_bit_mosi(i));
          end
          drive_last_bit(req.get_bit_mosi(req.item_len-1));
          //#(m_cfg.sck_period/2 - m_cfg.TSetup_mosi);
        end
        begin
          #m_cfg.THold_csb;
          sck_generate();
        end
      join_any
      disable fork;
      SPI.sck = m_cfg.cpol;
      #m_cfg.TSetup_csb;
      SPI.csb <= 1;
      SPI.mosi <= 'bz;
  endtask: drive_txn


endclass: spi_driver
