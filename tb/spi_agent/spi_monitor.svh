class spi_monitor extends uvm_monitor;
  `uvm_component_utils(spi_monitor)

  uvm_analysis_port #(bus_seq_item) ap;
  virtual spi_if SPI;
  bit coverage_enable = 1;
  spi_agent_config m_cfg;
  spi_seq_item item;

  // COVERAGE
  event trans_complete;

  covergroup spi_cov @(trans_complete);
    option.per_instance = 1;
    option.name = "spi_coverage";
    spi_phase : coverpoint m_cfg.cphase {
                                  bins phase_0 = {0};
                                  bins phase_1 = {1}; }

    spi_polarity : coverpoint m_cfg.cpol {
                                  bins polarity_0 = {0};
                                  bins polarity_1 = {1}; }

    cross_polarity_phase : cross spi_phase, spi_polarity;

    spi_MSB : coverpoint m_cfg.MSB {
                                  bins MSB_first = {1};
                                  bins LSB_first = {0}; }

    spi_txn_len : coverpoint (item.data_length%8) {
                                   bins standard = {0}; 
                                   bins error = {[1:7]};
                                  }

    spi_multibyte : coverpoint item.data_length {
                                  bins txn_singlebyte = {[0:16]};
                                  bins txn_multibyte = {[17:$]};
                               }

    spi_addr_cov : coverpoint item.addr {
                                    bins addr_0 = {7'h0};
                                    bins addr_1 = {7'h1};
                                    bins addr_2 = {7'h2};
                                    bins addr_3 = {7'h3};
                                    bins addr_4 = {7'h4};
                                    bins addr_5 = {7'h5};
                                    bins addr_6 = {7'h6};
                                    bins addr_7 = {7'h7};
                                }
    spi_dir : coverpoint item.write {
                              bins READ = {0};
                              bins WRITE = {1};
                            }
                            cross_addr_dir : cross spi_dir, spi_addr_cov;
  endgroup 

  function new(string name, uvm_component parent);
    super.new(name, parent);
    void'(uvm_config_int::get(this,"","coverage_enable",coverage_enable));
    if (coverage_enable) spi_cov = new();
  endfunction

  function void build_phase (uvm_phase phase);
    super.build_phase(phase);
    ap = new("ap", this);
    m_cfg = spi_agent_config::get_config(this);
  endfunction

  function void connect_phase (uvm_phase phase);
    super.connect_phase( phase);
    SPI = m_cfg.SPI;
  endfunction

  task run_phase (uvm_phase phase);
    fork
      collect_transaction();
      check_miso_stable_csb_high();
      check_miso_timing();
    join
  endtask

  task wait_active_sample_edge();
    if(m_cfg.cpol != m_cfg.cphase) @(negedge SPI.sck);
    else @(posedge SPI.sck);
  endtask

  task collect_transaction();
    spi_seq_item cloned_item;

    forever begin
      @(negedge SPI.csb);
      item = spi_seq_item::type_id::create("item");
      fork
        begin
          @(posedge SPI.csb);
        end
        begin
          forever begin
            wait_active_sample_edge();
            item.push_bit(SPI.mosi, SPI.miso);
          end
          wait_active_sample_edge();
        end
      join_any
      disable fork;
      item.deserialize();
      if(m_cfg.MSB == 0) item.reverse();
      `uvm_info("SPI MONITOR: ", item.to_string(), UVM_INFO)
      $cast(cloned_item, item.clone());
      ap.write(cloned_item);
      -> trans_complete;
    end
  endtask: collect_transaction

  task check_miso_stable_csb_high();
    forever begin
      wait(m_cfg.checks_enable);
      wait (SPI.csb);
      #5; 
      fork
        begin
          @(SPI.csb);
        end
        begin
          @(SPI.miso);
          `uvm_error("SPI MONITOR: ", $sformatf("Miso changed while SPI.csb is high."))
        end
      join_any
      disable fork;
    end
  endtask: check_miso_stable_csb_high

  task check_miso_timing();
    fork
      check_TSetup_miso();
      check_THold_miso();
    join
  endtask: check_miso_timing

  task check_TSetup_miso();
    integer event_time;
    forever begin
      wait(m_cfg.timing_checks_enable);
      @(SPI.miso);
      event_time = $time;
      fork
        begin
          #m_cfg.TSetup_miso;
        end
        begin
          wait_active_sample_edge();
          `uvm_error("SPI MONITOR: ", $sformatf("MISO setup time violation.\n   Expected: %d ns\n   Actual: %d ns\n(you can turn this check off by setting spi_agent_cfg.timing_checks_enable to 0.)",
                                                                         m_cfg.TSetup_miso, ($time-event_time)));
        end
      join_any
      disable fork;
    end
  endtask

  task check_THold_miso();
    integer event_time;
    forever begin
      wait(m_cfg.timing_checks_enable);
      wait_active_sample_edge();
      event_time = $time;
      fork
        begin
          #m_cfg.THold_miso;
        end
        begin
          @(SPI.miso);
          `uvm_error("SPI MONITOR: ", $sformatf("MISO hold time violation.\n   Expected: %d ns\n   Actual: %d ns\n(you can turn this check off by setting spi_agent_cfg.timing_checks_enable to 0.)",
                                                                         m_cfg.THold_miso, ($time-event_time)));
          `uvm_error("SPI MONITOR: ", "Miso hold time check failed");
        end
      join_any
      disable fork;
    end
  endtask
endclass

