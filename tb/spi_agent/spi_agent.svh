class spi_agent extends uvm_component;

`uvm_component_utils(spi_agent)

  // Data members
  spi_agent_config m_cfg;

  // Component members
  uvm_analysis_port #(bus_seq_item) ap;
  spi_monitor m_monitor;
  spi_sequencer m_sequencer;
  spi_driver m_driver;
  //spi_coverage_monitor m_fcov_monitor;

  // Methods

  // Standard UVM Methods:
  extern function new(string name = "spi_agent", uvm_component parent = null);
  extern function void build_phase (uvm_phase phase);
  extern function void connect_phase (uvm_phase phase);

endclass: spi_agent

function spi_agent::new(string name = "spi_agent", uvm_component parent = null);
  super.new(name, parent);
endfunction

function void spi_agent::build_phase(uvm_phase phase);
  if ( !uvm_config_db #(spi_agent_config)::get(this,"","spi_agent_config",m_cfg) ) begin
    `uvm_fatal("build_phase", "unable to get spi_agent_config")
  end
  m_monitor = spi_monitor::type_id::create("m_monitor",this);
  if (!m_cfg) begin
    `uvm_fatal("build_phase", "m_cfg null")
  end
  
  if (m_cfg.active == UVM_ACTIVE) begin
    m_driver = spi_driver::type_id::create("m_driver", this);
    m_sequencer = spi_sequencer::type_id::create("m_sequencer", this);
  end

//  if(m_cfg.has_functional_coverage) begin
//    m_fcov_monitor = spi_coverage_monitor::type_id::create("m_fcov_monitor", this);
//  end

endfunction: build_phase

function void spi_agent::connect_phase (uvm_phase phase);
  m_monitor.SPI = m_cfg.SPI;
  ap = m_monitor.ap;
  
  if(m_cfg.active == UVM_ACTIVE) begin
    m_driver.seq_item_port.connect(m_sequencer.seq_item_export);
    m_driver.SPI = m_cfg.SPI;
  end

  //if(m_cfg.has_functional_coverage) begin
  //  m_monitor.ap.connect(m_fcov_monitor.analysis_export);
  //end

endfunction: connect_phase

