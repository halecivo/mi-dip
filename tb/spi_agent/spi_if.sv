`include timescale.sv

interface spi_if;
  
  logic csb;
  logic sck;
  logic mosi;
  logic miso;

endinterface: spi_if
