class spi_seq_item extends bus_seq_item;

  `uvm_object_utils(spi_seq_item)

  `define MAX_BITSTREAM_LEN (`MAX_DATA_BITS+8)
  `define MAX_BITSTREAM_HI (`MAX_BITSTREAM_LEN-1)

  // data members
  logic[`MAX_BITSTREAM_HI:0] mosi;
  logic[`MAX_BITSTREAM_HI:0] miso;
  integer item_len = 0;

  function new(string name="spi_seq_item");
    super.new(name);
  endfunction

  extern function void serialize();
  extern function void deserialize();
  extern function logic[7:0] get_byte_mosi(int byte_num);
  extern function logic[7:0] get_byte_miso(int byte_num);
  extern function void push_bit(logic mosi, logic miso);
  extern function logic get_bit_mosi(int index);
  extern function logic get_bit_miso(int index);

  function int get_bytes_cnt ();
    if (item_len % 8) 
      return item_len/8 + 1;
    else
      return item_len/8;
  endfunction

endclass: spi_seq_item

function void spi_seq_item::serialize();
  mosi[`MAX_BITSTREAM_HI -: 8] = {addr, write};
  miso[`MAX_BITSTREAM_HI -: 8] = 8'h0;
  if (write) begin
    mosi[`MAX_DATA_HI:0] = data;
    miso[`MAX_DATA_HI:0] = 'h0;
  end
  else begin
    mosi[`MAX_DATA_HI:0] = 'h0;
    miso[`MAX_DATA_HI:0] = data;
  end
  item_len = data_length + 8;
endfunction

function void spi_seq_item::deserialize();
  {addr, write} = mosi[`MAX_BITSTREAM_HI -: 8];
  if (write) begin
    data = mosi[`MAX_DATA_HI:0];
  end
  else begin
    data = miso[`MAX_DATA_HI:0];
  end
  data_length = item_len - 8;
endfunction

function logic[7:0] spi_seq_item::get_byte_mosi(int byte_num);
  return mosi[(`MAX_BITSTREAM_HI-byte_num*8) -: 8];
endfunction: get_byte_mosi

function logic[7:0] spi_seq_item::get_byte_miso(int byte_num);
  return miso[(`MAX_BITSTREAM_HI-byte_num*8) -: 8];
endfunction: get_byte_miso

function void spi_seq_item::push_bit(logic mosi, logic miso);
  this.mosi[`MAX_BITSTREAM_HI-item_len] = mosi;
  this.miso[`MAX_BITSTREAM_HI-item_len] = miso;
  item_len++;
endfunction: push_bit

function logic spi_seq_item::get_bit_miso(int index);
  return miso[`MAX_BITSTREAM_HI-index];
endfunction: get_bit_miso

function logic spi_seq_item::get_bit_mosi(int index);
  return mosi[`MAX_BITSTREAM_HI-index];
endfunction: get_bit_mosi
