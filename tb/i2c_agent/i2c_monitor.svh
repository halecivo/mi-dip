class i2c_monitor extends uvm_monitor;
  `uvm_component_utils(i2c_monitor)

  uvm_analysis_port #(bus_seq_item) ap;
  virtual i2c_if I2C;
  bit coverage_enable = 1;
  i2c_agent_config m_cfg;

  i2c_seq_item item;
  bit wait_for_start = 1;

  // COVERAGE
  event trans_complete;

  covergroup i2c_cov @(trans_complete);
    option.per_instance = 1;
    option.name = "i2c_coverage";
    i2c_MSB : coverpoint m_cfg.MSB {
                                  bins MSB_first = {1};
                                  bins LSB_first = {0}; }
    i2c_txn_len : coverpoint (item.data_length%8) {
                                   bins standard = {0}; 
                                   bins error = {[1:7]};
                                  }

    i2c_multibyte : coverpoint item.data_length {
                                  bins txn_singlebyte = {[0:16]};
                                  bins txn_multibyte = {[17:$]};
                               }
  endgroup 

  function new(string name, uvm_component parent);
    super.new(name, parent);
    void'(uvm_config_int::get(this,"","coverage_enable",coverage_enable));
    if (coverage_enable) i2c_cov = new();
  endfunction

  function void build_phase (uvm_phase phase);
    super.build_phase(phase);
    ap = new("ap", this);
    m_cfg = i2c_agent_config::get_config(this);
  endfunction

  function void connect_phase (uvm_phase phase);
    super.connect_phase( phase);
    I2C = m_cfg.I2C;
  endfunction

  task run_phase (uvm_phase phase);
    fork
      collect_transactions();
      check_sda_timing();
    join

  endtask

  task check_sda_timing();
    fork
       check_TSetup_sda();
       check_THold_sda();
    join
  endtask

  task check_TSetup_sda();
    integer event_time;
    forever begin
      wait(m_cfg.timing_checks_enable);
        wait(I2C.scl_i == 0);
        @(I2C.sda_i);
        event_time = $time;
        fork
          begin
            #m_cfg.Tmin_Setup_sda;
          end
          begin
            @(I2C.scl_i);
            `uvm_error("I2C MONITOR: ", $sformatf("SDA setup time violation.\n   Expected: %d ns\n   Actual: %d ns\n(you can turn this check off by setting i2c_agent_cfg.timing_checks_enable to 0.)",
                                                                         m_cfg.Tmin_Setup_sda, ($time-event_time)));
          end
        join_any
        disable fork;
      end
  endtask

  task check_THold_sda();
    integer event_time;
    forever begin
      wait(m_cfg.timing_checks_enable);
        @(negedge I2C.scl_i);
        event_time = $time;
        fork
          begin
            #m_cfg.Tmin_Hold_sda;
          end
          begin
            @(I2C.sda_i);
            `uvm_error("I2C MONITOR: ", $sformatf("SDA hold time violation.\n   Expected: %d ns\n   Actual: %d ns\n(you can turn this check off by setting i2c_agent_cfg.timing_checks_enable to 0.)",
                                                                         m_cfg.Tmin_Hold_sda, ($time-event_time)));
          end
        join_any
        disable fork;
      end
  endtask


  task collect_transactions();
    i2c_seq_item cloned_item;

    forever begin
      // one register transaction is divided to two i2c transactions 
      if (wait_for_start) detect_start();
      item = i2c_seq_item::type_id::create("item");
      collect_txn();
      if (wait_for_start) detect_start();
      collect_txn(1);
      item.deserialize();
      if((item.slave_addr[6:0] == m_cfg.dut_slave_address) || m_cfg.ignore_slave_address) begin
        if(m_cfg.MSB == 0) item.reverse();
        `uvm_info("I2C MONITOR: ", item.to_string(), UVM_INFO)
        $cast(cloned_item, item.clone());
        ap.write(cloned_item);
      end
      else begin
        `uvm_warning("I2C MONITOR: ", $sformatf("Transaction slave address (%x) differs from DUT slave address (%x), so it won't be send to scoreboard. \n \
                                       Set cfg.dut_slave_address to %x or cfg.ignore_slave_address to 1 to let this transaction pass.", item.slave_addr[6:0], m_cfg.dut_slave_address, 
                                     item.slave_addr[6:0]))
      end
      -> trans_complete;
    end

  endtask: collect_transactions

  task collect_txn(bit skip_addr = 0);
    logic sampled_bit;
    fork
      begin
        @(posedge I2C.scl_i); // 1 bit delay in pushing to transaction, because last bit is part of stop bit
        sampled_bit = I2C.sda_i;
        forever begin
          @(posedge I2C.scl_i);
          item.push_bit(sampled_bit);
          sampled_bit = I2C.sda_i;
        end
      end
      begin
        detect_stop();
        wait_for_start = 1;
      end
      begin
        detect_start();
        wait_for_start = 0;
      end
    join_any
    disable fork;
  endtask

  task get_slave_addr();
    logic [6:0] slave_addr;
    for(int i = 6; i >= 0; i--) begin
      @(posedge I2C.scl_i);
      slave_addr[i] = I2C.sda_i;
    end
    if (slave_addr[6:2] == `SLAVE_ADDR_10B_PREFIX)
      `uvm_error("SPI MONITOR: ", "Detected 10bit address prefix, but 10bit addressing is not supported by UVC")
  endtask

  task get_write_bit();
    @(posedge I2C.scl_i);
    item.write = ~I2C.sda_i;
  endtask

  task detect_start();
    @(negedge I2C.sda_i);
    while (!I2C.scl_i) @(negedge I2C.sda_i);
    //`uvm_info("I2C MONITOR: ", "detected start", UVM_NONE);
  endtask

  task detect_stop();
    @(posedge I2C.sda_i);
    while (!I2C.scl_i) @(posedge I2C.sda_i);
    //`uvm_info("I2C MONITOR: ", "detected stop", UVM_NONE);
  endtask

endclass

