class i2c_seq extends uvm_sequence #(i2c_seq_item);

  `uvm_object_utils(i2c_seq)

  extern function new(string name = "spi_seq");
  extern task body;

endclass: i2c_seq


function i2c_seq::new(string name = "i2c_seq");
  super.new(name);
endfunction

task i2c_seq::body;
  i2c_seq_item req;

  begin
    for (int i = 0; i < 10; i++) begin
      req = i2c_seq_item::type_id::create("req");
      start_item(req);
      assert(req.randomize() with {
        addr != 7'h07;
        item_length == 27;
      });
      req.set_slave_addr_7b(7'h44);
      //req.push_byte(8'h5c);
      //req.data = 8'h5c;
      finish_item(req);
      start_item(req);
      req.write = 0;
      finish_item(req);
    end
  end

endtask: body
