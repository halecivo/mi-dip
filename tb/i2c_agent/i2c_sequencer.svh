class i2c_sequencer extends uvm_sequencer #(bus_seq_item, bus_seq_item);

  `uvm_component_utils(i2c_sequencer)

  extern function new(string name="i2c_sequencer", uvm_component parent = null);

endclass: i2c_sequencer

function i2c_sequencer::new(string name="i2c_sequencer", uvm_component parent = null);
  super.new(name, parent);
endfunction
