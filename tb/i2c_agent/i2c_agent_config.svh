// Class Description:

class i2c_agent_config extends uvm_object;

  localparam string s_my_config_id = "i2c_agent_config";
  localparam string s_no_config_id = "no config";
  localparam string s_my_config_type_error_id = "config type error";

  // UVM Factory Registration Macro
  `uvm_object_utils(i2c_agent_config)

  // Virtual Interface 
  virtual i2c_if I2C;

  // Data Members

  // Is the agent active or pasive
  uvm_active_passive_enum active = UVM_ACTIVE;
  // Include the I2C functional coverage monitor
  bit has_functional_coverage = 1;

  // i2c configuration
  bit MSB = 1; // 1 = MSB first
  logic [6:0] dut_slave_address = 7'h44;
  bit ignore_slave_address = 0; // if set to 0, only transactions with slave address == dut_slave_address are sent to scoreboard, others ignored. If set to 1, all transactions are sent to SB.

  // timing config
  time TSetup_stop  = 4.0us;
  time THold_start  = 4.0us;
  time TSetup_start = 4.7us;
  time TSetup_sda   = 0.25us;
  time THold_sda    = 0.10us;
  time Tbuf         = 47us;
  time Tscl_low     = 5.0us;
  time Tscl_high    = 15.0us;

  // checks config
  bit checks_enable = 1;
  bit timing_checks_enable = 1;
  time Tmin_Setup_sda   = 0.25us;
  time Tmin_Hold_sda    = 0.01us;

  // Methods
  extern static function i2c_agent_config get_config(uvm_component c);
  extern function new(string name = "i2c_agent_config");

endclass: i2c_agent_config

  function i2c_agent_config::new(string name = "i2c_agent_config");
    super.new(name);
  endfunction

  function i2c_agent_config i2c_agent_config::get_config(uvm_component c);
    i2c_agent_config t;
    if(!uvm_config_db #(i2c_agent_config)::get(c, "", s_my_config_id, t)) begin
    `uvm_fatal("CONFIG_LOAD", $sformatf("Cannot get() configuration %s from uvm_config_db. Have you set() it?", s_my_config_id))
  end

    return t;
  endfunction: get_config
