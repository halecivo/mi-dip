class i2c_driver extends uvm_driver #(bus_seq_item);
  `uvm_component_utils(i2c_driver)

  `define IS_FIRST_BYTE i == 0
  `define IS_LAST_BYTE i == (byte_num - 1)

  bus_seq_item item;
  i2c_seq_item req;
  i2c_agent_config m_cfg;

  virtual i2c_if I2C;

  function new (string name = "i2c_driver", uvm_component parent = null);
    super.new(name, parent);
  endfunction

  virtual function void build_phase (uvm_phase phase);
    super.build_phase(phase);
    if (m_cfg == null) begin
        if(!uvm_config_db #(i2c_agent_config)::get(this, "", "i2c_agent_config", m_cfg)) begin
          `uvm_fatal("CONFIG_LOAD", $sformatf("Cannot get() configuration %s from uvm_config_db. Have you set() it?", "i2c_agent_config"))
        end
    end
  endfunction: build_phase

  task reset_phase (uvm_phase phase);
    i2c_seq_item req = i2c_seq_item::type_id::create("req");
    I2C.scl_o = 'b1;
    I2C.sda_o = 'b1;
  endtask: reset_phase
    

  task run_phase (uvm_phase phase);

    // Default conditions:
    I2C.scl_o = 'b1;
    I2C.sda_o = 'b1;

    req = i2c_seq_item::type_id::create("i2c_seq_item");

    forever begin
      seq_item_port.get_next_item(item);
      req.do_copy(item);
      if(m_cfg.MSB == 0) req.reverse();
      req.set_slave_addr_7b(m_cfg.dut_slave_address);
      req.serialize();
      `uvm_info("I2C DRIVER: ", req.to_string(), UVM_INFO)
      drive_txn(req);
      seq_item_port.item_done();
      #m_cfg.Tbuf; // wait for at least bus free time
    end

  endtask: run_phase

  task drive_bit(logic b, bit drive_x = 1);
    //`uvm_info("I2C DRIVER: ", $sformatf("driving bit %x", b), UVM_INFO)
    #m_cfg.THold_sda;
    if (drive_x) I2C.sda_o <= 1'bx;
    #(m_cfg.Tscl_low - m_cfg.TSetup_sda - m_cfg.THold_sda);
    I2C.sda_o <= b;
    #m_cfg.TSetup_sda;
    I2C.scl_o <= 1'b1;
    #m_cfg.Tscl_high;
    I2C.scl_o <= 1'b0;
  endtask: drive_bit

  task drive_start();
    I2C.sda_o <= 1'b0;
    #m_cfg.THold_start;
    I2C.scl_o <= 1'b0;
  endtask: drive_start

  task drive_stop();
    #m_cfg.THold_sda;
    I2C.sda_o <= 1'b0;
    #(m_cfg.Tscl_low - m_cfg.THold_sda);
    I2C.scl_o <= 1'b1;
    #m_cfg.TSetup_stop;
    I2C.sda_o <= 1'b1;
  endtask: drive_stop

  task drive_stop_n_start();
    drive_stop();
    #m_cfg.Tbuf;
    drive_start();
  endtask: drive_stop_n_start

  task drive_repeated_start();
    #m_cfg.THold_sda;
    I2C.sda_o <= 1'b1;
    #(m_cfg.Tscl_low - m_cfg.THold_sda);
    I2C.scl_o <= 1'b1;
    #m_cfg.TSetup_start;
    drive_start();
  endtask: drive_repeated_start

  task drive_txn(i2c_seq_item req);
      drive_start();
      for (int i = 0; i < 18; i++) begin
        drive_bit(req.get_bit(i));
      end
      drive_stop_n_start();
      for (int i = 18; i < req.item_length; i++) begin
        drive_bit(req.get_bit(i), req.write);
      end
      drive_stop();
  endtask: drive_txn


endclass: i2c_driver
