class i2c_seq_item extends bus_seq_item;

  `uvm_object_utils(i2c_seq_item)

  `define MAX_BITSTREAM_LEN (((`MAX_DATA_BITS + 23) / 8)*9)
  `define MAX_BITSTREAM_HI (`MAX_BITSTREAM_LEN-1)

  // data members
  logic [9:0] slave_addr; // slave address - 7 bit address is located in [6:0]
  int slave_addr_length = 7;
  int item_length = 0;
  logic[`MAX_BITSTREAM_HI:0] bitstream;

  function void set_slave_addr_10b(logic[9:0] addr);
    slave_addr = addr;
    slave_addr_length = 10;    
  endfunction

  function void set_slave_addr_7b(logic[6:0] addr);
    slave_addr = {3'bx, addr};
    slave_addr_length = 7;
  endfunction

  function new(string name="i2c_seq_item");
    super.new(name);
  endfunction

  extern function void serialize();
  extern function void deserialize();
  extern function void push_bit(logic b);
  extern function logic get_bit(int bit_num);

  function int get_bytes_cnt ();
    if (item_length % 9) 
      return item_length/9;
    else
      return item_length/9-1;
  endfunction

endclass: i2c_seq_item

function void i2c_seq_item::serialize();
  bitstream[`MAX_BITSTREAM_HI -: 27] = {slave_addr[6:0], 1'b0, 1'b1, addr, write, 1'b1, slave_addr[6:0], ~write, 1'b1};
  if (write) begin
    for (int i = 0; i < (get_data_bytes_cnt()-1); i++) begin
      bitstream[`MAX_BITSTREAM_HI-9*(i+3) -: 9] = {get_data_byte(i), 1'b1}; 
    end
    bitstream[`MAX_BITSTREAM_HI-9*(get_data_bytes_cnt+2) -: 9] = {get_data_byte(get_data_bytes_cnt()-1), 1'b0}; 
  end
  else begin
    for (int i = 0; i < (get_data_bytes_cnt()-1); i++) begin
      bitstream[`MAX_BITSTREAM_HI-9*(i+3) -: 9] = {8'hFF, 1'b0}; 
    end
    bitstream[`MAX_BITSTREAM_HI-9*(get_data_bytes_cnt+2) -: 9] = {8'hFF, 1'b1}; 
  end
  //$display($sformatf("dri stream %b   %b", bitstream[`MAX_BITSTREAM_HI -: 27], bitstream[`MAX_BITSTREAM_HI-27:0] ));
  item_length = (data_length/8)*9 + (data_length%8) + 27;
endfunction: serialize

function void i2c_seq_item::deserialize();
  data_length = ((item_length/9)-3)*8 + (item_length%9);
  // 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30  ~
  // <   address   > X A <      reg addr       > WR  A <   address        > WE  A < data ~ 
  slave_addr[6:0] = bitstream[`MAX_BITSTREAM_HI -: 7];
  //$display($sformatf("mon stream %b %b %b", bitstream[`MAX_BITSTREAM_HI -: 27],bitstream[`MAX_BITSTREAM_HI-25], bitstream[`MAX_BITSTREAM_HI-27:0] ));
  
  write = ~bitstream[`MAX_BITSTREAM_HI-25];
  addr = bitstream[(`MAX_BITSTREAM_HI-9) -: `ADDR_LEN];
  for (int i = 0; i < get_bytes_cnt(); i++) begin
    data[(`MAX_DATA_HI-i*8) -: 8] = bitstream[`MAX_BITSTREAM_HI-((i+3)*9) -: 8];
  end
endfunction: deserialize

function void i2c_seq_item::push_bit(logic b);
  bitstream[`MAX_BITSTREAM_HI-item_length] = b;
  item_length++;
endfunction

function logic i2c_seq_item::get_bit(int bit_num);
  if (bit_num >= item_length) begin
    `uvm_error("I2C_SEQ_ITEM: ", $sformatf("Requested bit from outside of bitstream. Item_length: %d, requested bit: %d.", item_length, bit_num))
    return 1'bx;
  end
  else begin
    return bitstream[`MAX_BITSTREAM_HI-bit_num];
  end
endfunction
