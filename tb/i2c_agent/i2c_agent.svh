class i2c_agent extends uvm_component;

`uvm_component_utils(i2c_agent)

  // Data members
  i2c_agent_config m_cfg;

  // Component members
  uvm_analysis_port #(bus_seq_item) ap;
  i2c_monitor m_monitor;
  i2c_sequencer m_sequencer;
  i2c_driver m_driver;
  //i2c_coverage_monitor m_fcov_monitor;

  // Methods

  // Standard UVM Methods:
  extern function new(string name = "i2c_agent", uvm_component parent = null);
  extern function void build_phase (uvm_phase phase);
  extern function void connect_phase (uvm_phase phase);

endclass: i2c_agent

function i2c_agent::new(string name = "i2c_agent", uvm_component parent = null);
  super.new(name, parent);
endfunction

function void i2c_agent::build_phase(uvm_phase phase);
  if ( !uvm_config_db #(i2c_agent_config)::get(this,"","i2c_agent_config",m_cfg) ) begin
    `uvm_fatal("build_phase", "unable to get i2c_agent_config")
  end
  m_monitor = i2c_monitor::type_id::create("m_monitor",this);
  if (!m_cfg) begin
    `uvm_fatal("build_phase", "m_cfg null")
  end
  
  if (m_cfg.active == UVM_ACTIVE) begin
    m_driver = i2c_driver::type_id::create("m_driver", this);
    m_sequencer = i2c_sequencer::type_id::create("m_sequencer", this);
  end

//  if(m_cfg.has_functional_coverage) begin
//    m_fcov_monitor = i2c_coverage_monitor::type_id::create("m_fcov_monitor", this);
//  end

endfunction: build_phase

function void i2c_agent::connect_phase (uvm_phase phase);
  m_monitor.I2C = m_cfg.I2C;
  ap = m_monitor.ap;
  
  if(m_cfg.active == UVM_ACTIVE) begin
    m_driver.seq_item_port.connect(m_sequencer.seq_item_export);
    m_driver.I2C = m_cfg.I2C;
  end

  //if(m_cfg.has_functional_coverage) begin
  //  m_monitor.ap.connect(m_fcov_monitor.analysis_export);
  //end

endfunction: connect_phase

