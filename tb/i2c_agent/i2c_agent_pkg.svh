package i2c_agent_pkg;

import uvm_pkg::*;
import bus_txn_pkg::*;
`include "uvm_macros.svh"

`define SLAVE_ADDR_10B_PREFIX 5'b11110

`include "i2c_seq_item.svh"
`include "i2c_agent_config.svh"
`include "i2c_driver.svh"
//`include "spi_coverage_monitor.svh"
`include "i2c_monitor.svh"
`include "i2c_sequencer.svh"
`include "i2c_agent.svh"

//`include "i2c_seq.svh"

endpackage: i2c_agent_pkg
