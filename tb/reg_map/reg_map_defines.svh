bit[6:0]   REG_INFO      =  7'h0;
bit[6:0]   REG_CNT       =  7'h1;
bit[6:0]   REG_ADC       =  7'h2;
bit[6:0]   REG_ST        =  7'h3;
bit[6:0]   REG_MEAS      =  7'h4;
bit[6:0]   REG_CONST     =  7'h5;
bit[6:0]   REG_FREE1     =  7'h6;
bit[6:0]   REG_BUS_SETUP =  7'h7;
