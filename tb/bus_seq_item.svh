class bus_seq_item extends uvm_sequence_item;
  
  `uvm_object_utils(bus_seq_item)

  `define ADDR_LEN 7
  `define MAX_DATA_BITS 80

  `define ADDR_HI (`ADDR_LEN-1)
  `define MAX_DATA_HI (`MAX_DATA_BITS-1)

  // data members
  rand int data_length = 0; // length of transaction (including addr and write bit), in number of bits
  rand logic [`ADDR_HI:0] addr;
  rand logic [`MAX_DATA_HI:0] data;
  rand bit write;

  function new(string name="bus_seq_item");
    super.new(name);
  endfunction

  function void serialize();
  endfunction
  function void deserialize();
  endfunction


  constraint c_default {
    addr[6:3] == 'b0;
    data_length > 0;
    data_length < `MAX_DATA_BITS;
  }

  function string to_string();
    string data_str = "";
    for(int i = 0; i < data_length; i += 8) begin
      data_str = {data_str, $sformatf("%x",data[(`MAX_DATA_HI-i) -: 8])};
    end
    return $sformatf("[%s] ADDR: %x, DATA: %s, LEN: %d", (write) ? "WRITE" : "READ", addr, data_str, data_length);
  endfunction
  
  function int get_data_bytes_cnt();
    if (data_length % 8) 
      return data_length/8 + 1;
    else
      return data_length/8;
  endfunction

  function logic[7:0] get_data_byte(int index);
    return data[`MAX_DATA_HI - 8*index -: 8];
  endfunction

  function void push_byte(logic [7:0] txn_byte, int num_bits = 8);
    if (num_bits > 8) `uvm_error("BUS_SEQ_ITEM:", $sformatf("trying to push byte with more than 8 bits (%d)", num_bits))
    data[(`MAX_DATA_HI-data_length) -: 8] = txn_byte;
    data_length += num_bits;
  endfunction: push_byte

  function void reverse();
    addr = {<<{addr}}; 
    for (int i = 0; i < get_data_bytes_cnt(); i++) begin
      data[(`MAX_DATA_HI-i*8) -: 8] = {<<{get_data_byte(i)}};
    end
    // shift last byte
    if(data_length % 8)
      data[(`MAX_DATA_HI-(get_data_bytes_cnt()-1)*8) -: 8] = data << (8 - (data_length % 8) ); // shift last data bits ([x x x b4 b3 b2 b1 b0]  -> [b0 b1 b2 b3 b4 0 0 0])
  endfunction

  function void do_copy(uvm_object rhs);
    bus_seq_item rhs_;

    if(!$cast(rhs_, rhs)) begin
      `uvm_fatal("do_copy", "cast of rhs object failed")
    end
    super.do_copy(rhs);
    // Copy over data members:
    addr = rhs_.addr;
    write = rhs_.write;
    data = rhs_.data;
    data_length = rhs_.data_length;

  endfunction:do_copy

endclass: bus_seq_item

