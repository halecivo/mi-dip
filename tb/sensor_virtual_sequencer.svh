class sensor_virtual_sequencer extends uvm_sequencer #(uvm_sequence_item);

  `uvm_component_utils(sensor_virtual_sequencer)

  spi_sequencer spi;
  i2c_sequencer i2c;

  extern function new(string name = "sensor_virtual_sequencer", uvm_component parent = null);

endclass: sensor_virtual_sequencer

function sensor_virtual_sequencer::new(string name = "sensor_virtual_sequencer", uvm_component parent = null);
  super.new(name,parent);
endfunction

