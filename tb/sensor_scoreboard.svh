class sensor_scoreboard extends uvm_component;

  `uvm_component_utils(sensor_scoreboard)

  uvm_tlm_analysis_fifo #(bus_seq_item) spi;
  uvm_tlm_analysis_fifo #(bus_seq_item) i2c;
  uvm_tlm_analysis_fifo #(adc_seq_item) adc;

  sensor_env_config m_cfg;

  //uvm_reg_map sensor_rm;
  logic [63:0] sensor_regs = 64'b0;
  logic [7:0] prev_adc_value;
  logic [7:0] ro_flags = 8'b00011100; // read only register flags

  // statistics
  int no_transfers;
  int no_spi_transfers;
  int no_i2c_transfers;
  int no_tx_errors;
  int no_cs_errors;
  int no_adc_values;

  function new(string name = "", uvm_component parent = null);
    super.new(name, parent);
  endfunction

  function void build_phase(uvm_phase phase);
    spi = new("spi", this);
    i2c = new("i2c", this);
    adc = new("adc", this);
  endfunction: build_phase

  task run_phase(uvm_phase phase);
    no_transfers = 0;
    no_spi_transfers = 0;
    no_i2c_transfers = 0;
    no_tx_errors = 0;
    no_cs_errors = 0;
    no_adc_values = 0; 

  

    fork
      track_spi;
      track_i2c;
      track_adc; 
    join
  endtask: run_phase

  task track_adc;
    adc_seq_item item;
    logic[5:0] meas_add;
    logic[1:0] meas_shift;

    forever begin
      adc.get(item);
      no_adc_values++;
      register_write(7'h2, item.data,1);
      register_write(7'h3, 8'h01,1);
      {meas_shift, meas_add} = register_get(7'h5);
      register_write(7'h4, meas_add + (item.data >> meas_shift), 1);
    end
  endtask: track_adc


  task track_spi;
    bus_seq_item item;

    bit error;

    forever begin
      error = 0;
      spi.get(item);
      no_transfers++;
      no_spi_transfers++;

      if (item.write) begin
        for(int i = 0; i < item.get_data_bytes_cnt(); i++)
          register_write(item.addr+i, item.get_data_byte(i));
      end
      else begin
        for(int i = 0; i < item.get_data_bytes_cnt(); i++)
          register_check(item.addr+i, item.get_data_byte(i));
      end

    end
  endtask: track_spi

  task track_i2c;
    bus_seq_item item;
    bit error;
    int no_txn_bytes;

    register_write(7'h05, 8'hF0, 1);

    forever begin
      error = 0;
      i2c.get(item);
      no_transfers++;
      no_i2c_transfers++;

      no_txn_bytes = item.get_data_bytes_cnt();
      if (no_txn_bytes < 1) begin
        `uvm_error("SENSOR_SB:", $sformatf("transaction with zero transaction length received."))
        no_tx_errors++;
      end

      if (item.write) begin
        for(int i = 0; i < no_txn_bytes; i++) begin
          register_write(item.addr, item.get_data_byte(i));
        end
      end
      else begin
        for(int i = 0; i < no_txn_bytes; i++) begin
          register_check(item.addr+i, item.get_data_byte(i));
        end
      end

    end
  endtask: track_spi

  function void report_phase(uvm_phase phase);
    `uvm_info("SENSOR_SB:", $sformatf("Scoreboard report: \n\
                transactions:  %d\n\
                errors:        %d\n\
                success rate: %d\%", no_transfers, no_tx_errors, (100*(no_transfers - no_tx_errors))/no_transfers), UVM_INFO);
  endfunction: report_phase

  // frc - force write even to RO register (used when write is requested by sensor, not bus transaction)
  function void register_write(logic[6:0] addr, logic[7:0] data, bit frc = 0);
    if(!m_cfg.enable_scoreboard_checks) return;
    if (ro_flags[addr[2:0]] == 1 && !frc) begin
      `uvm_info ("SENSOR_SB:", $sformatf("write to RO register (addr = %x) rejected", addr), UVM_INFO)
      return;
    end
    case(addr[2:0])
      3'b000: sensor_regs[7:0] = data;
      3'b001: sensor_regs[15:8] = data;
      3'b010: begin 
           prev_adc_value = sensor_regs[23:16]; 
           sensor_regs[23:16] = data;
        end
      3'b011: sensor_regs[31:24] = data;
      3'b100: sensor_regs[39:32] = data;
      3'b101: sensor_regs[47:40] = data;
      3'b110: sensor_regs[55:48] = data;
      3'b111: sensor_regs[63:56] = data;
      default: begin
        `uvm_error("SENSOR_SB:", $sformatf("write access to unknown address %x", addr))
        no_tx_errors++;
      end
    endcase
      `uvm_info ("SENSOR_SB:", $sformatf("register write: addr = %x, data = %x", addr, data), UVM_INFO)
  endfunction

  function logic [7:0] register_get(logic[6:0] addr);
    case(addr[2:0])
      3'b000: return sensor_regs[7:0];
      3'b001: return sensor_regs[15:8];
      3'b010: return sensor_regs[23:16];
      3'b011: return sensor_regs[31:24];
      3'b100: return sensor_regs[39:32];
      3'b101: return sensor_regs[47:40];
      3'b110: return sensor_regs[55:48];
      3'b111: return sensor_regs[63:56];
    endcase
  endfunction


  function void register_check(logic[6:0] addr, logic[7:0] data);
    bit match = 0;
    if(!m_cfg.enable_scoreboard_checks) return;
    if(m_cfg.enable_LSB_first_check) begin
      addr = {<<{addr}};
      data = {<<{data}};
    end
    case(addr[2:0])
      3'b000: match = (sensor_regs[7:0]   == data);
      3'b001: match = (sensor_regs[15:8]  == data);
      3'b010: match = (sensor_regs[23:16] == data || prev_adc_value == data);
      3'b011: match = (sensor_regs[31:24] == data);
      3'b100: match = (sensor_regs[39:32] == data);
      3'b101: match = (sensor_regs[47:40] == data);
      3'b110: match = (sensor_regs[55:48] == data);
      3'b111: match = (sensor_regs[63:56] == data);
    endcase
    if (match) begin
      `uvm_info("SENSOR_SB:", $sformatf("register read: addr = %x, data = %x", addr, data), UVM_INFO)
    end
    else begin
      `uvm_error("SENSOR_SB:", $sformatf("register value different than expected!\n\
                      addr = %x\n\
                      data = %x\n\
                      expected = %x", addr, data, register_get(addr))) 
      no_tx_errors++;
    end
  endfunction

endclass: sensor_scoreboard
