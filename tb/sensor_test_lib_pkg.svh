package sensor_test_lib_pkg;
  import uvm_pkg::*;
  `include "uvm_macros.svh"
  

  import sensor_env_pkg::*;
  import spi_agent_pkg::*;
  import i2c_agent_pkg::*;
  import adc_agent_pkg::*;
  import bus_txn_pkg::*;

  `include "reg_map_defines.svh"
  
  //import bus_virtual_seq_lib_pkg::*;
  `include "base_reg_seq.svh"
  `include "spi_cfg_seq.svh"
  `include "bus_err_seq.svh"
  `include "multi_reg_seq.svh"
  `include "simple_reg_seq.svh"

  `include "sensor_test_base.svh"
  `include "spi_cfg_00_test.svh"
  `include "spi_cfg_01_test.svh"
  `include "spi_cfg_10_test.svh"
  `include "spi_cfg_11_test.svh"
  `include "spi_err_test.svh"
  `include "spi_timing_checks_test.svh"
  `include "multi_read_test.svh"
  `include "i2c_multi_read_test.svh"
  `include "lsb_first_test.svh"
  `include "i2c_lsb_first_test.svh"
  `include "i2c_test.svh"
  `include "i2c_err_test.svh"
  `include "i2c_timing_checks_test.svh"

  
endpackage: sensor_test_lib_pkg;
