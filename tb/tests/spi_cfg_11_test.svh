class spi_cfg_11_test extends sensor_test_base;

  `uvm_component_utils(spi_cfg_11_test)

  extern function new(string name = "spi_cfg_11_test", uvm_component parent = null);
  extern function void build_phase(uvm_phase phase);
  extern task reset_phase(uvm_phase phase);
  extern task main_phase(uvm_phase phase);

endclass: spi_cfg_11_test

function spi_cfg_11_test::new(string name = "spi_cfg_11_test", uvm_component parent = null);
  super.new(name, parent);
endfunction

function void spi_cfg_11_test::build_phase(uvm_phase phase);
  super.build_phase(phase);
endfunction: build_phase

task spi_cfg_11_test::reset_phase(uvm_phase phase);
  //check_reset_seq reset_test_seq = check_reset_seq::type_id::create("reset_test_seq");

  phase.raise_objection(this, "Starting reset_test_seq in reset phase");
  //reset_test_seq.start(m_env.m_v_sqr.spi);
  #m_env_cfg.dut_setup_time;
  phase.drop_objection(this, "Finished reset_test_seq in reset phase");
endtask: reset_phase

task spi_cfg_11_test::main_phase(uvm_phase phase);
  spi_cfg_seq     spi_set_seq = spi_cfg_seq::type_id::create("spi_cfg_seq");
  base_reg_seq     char_seq  = base_reg_seq::type_id::create("char_seq");
  spi_set_seq.cphase = 1;
  spi_set_seq.cpol = 1;

  phase.raise_objection(this, "Starting spi_seq in main phase");
  spi_set_seq.start(m_env.m_v_sqr.spi);
  m_spi_cfg.cphase = 1;
  m_spi_cfg.cpol = 1;
  char_seq.start(m_env.m_v_sqr.spi);
  phase.drop_objection(this, "Finished spi_seq in main phase");
endtask: main_phase
