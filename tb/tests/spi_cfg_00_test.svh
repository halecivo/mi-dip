class spi_cfg_00_test extends sensor_test_base;

  `uvm_component_utils(spi_cfg_00_test)

  extern function new(string name = "spi_cfg_00_test", uvm_component parent = null);
  extern function void build_phase(uvm_phase phase);
  extern task reset_phase(uvm_phase phase);
  extern task main_phase(uvm_phase phase);

endclass: spi_cfg_00_test

function spi_cfg_00_test::new(string name = "spi_cfg_00_test", uvm_component parent = null);
  super.new(name, parent);
endfunction

function void spi_cfg_00_test::build_phase(uvm_phase phase);
  super.build_phase(phase);
endfunction: build_phase

task spi_cfg_00_test::reset_phase(uvm_phase phase);
  //check_reset_seq reset_test_seq = check_reset_seq::type_id::create("reset_test_seq");

  phase.raise_objection(this, "Starting reset_test_seq in reset phase");
  //reset_test_seq.start(m_env.m_v_sqr.spi);
  #m_env_cfg.dut_setup_time;
  phase.drop_objection(this, "Finished reset_test_seq in reset phase");
endtask: reset_phase

task spi_cfg_00_test::main_phase(uvm_phase phase);
  spi_cfg_seq     spi_set_seq = spi_cfg_seq::type_id::create("spi_cfg_seq");
  base_reg_seq     char_seq  = base_reg_seq::type_id::create("char_seq");
  spi_set_seq.cphase = 0;
  spi_set_seq.cpol = 0;

  phase.raise_objection(this, "Starting char_seq in main phase");
  spi_set_seq.start(m_env.m_v_sqr.spi);
  m_spi_cfg.cphase = 0;
  m_spi_cfg.cpol = 0;
  char_seq.start(m_env.m_v_sqr.spi);
  phase.drop_objection(this, "Finished char_seq in main phase");
endtask: main_phase
