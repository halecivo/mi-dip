class spi_err_test extends sensor_test_base;

  `uvm_component_utils(spi_err_test)

  extern function new(string name = "spi_err_test", uvm_component parent = null);
  extern function void build_phase(uvm_phase phase);
  extern task reset_phase(uvm_phase phase);
  extern task main_phase(uvm_phase phase);

endclass: spi_err_test

function spi_err_test::new(string name = "spi_err_test", uvm_component parent = null);
  super.new(name, parent);
endfunction

function void spi_err_test::build_phase(uvm_phase phase);
  super.build_phase(phase);
endfunction: build_phase

task spi_err_test::reset_phase(uvm_phase phase);
  //check_reset_seq reset_test_seq = check_reset_seq::type_id::create("reset_test_seq");

  phase.raise_objection(this, "Starting reset_test_seq in reset phase");
  //reset_test_seq.start(m_env.m_v_sqr.spi);
  #m_env_cfg.dut_setup_time;
  phase.drop_objection(this, "Finished reset_test_seq in reset phase");
endtask: reset_phase

task spi_err_test::main_phase(uvm_phase phase);
  bus_err_seq     err_seq = bus_err_seq::type_id::create("bus_err_seq");
  base_reg_seq     char_seq  = base_reg_seq::type_id::create("char_seq");
  char_seq.num_txn = 3;

  phase.raise_objection(this, "Error transactions");
  `uvm_info("SPI_ERR_TEST:", "Disabling scoreboard checks and sending err sequence.", UVM_INFO)
  m_env_cfg.enable_scoreboard_checks = 0;
  err_seq.start(m_env.m_v_sqr.spi);
  phase.drop_objection(this, "Error transactions");
  phase.raise_objection(this, "Starting char_seq in main phase");
  `uvm_info("SPI_ERR_TEST:", "Enabling scoreboard checks and sending char sequence.", UVM_INFO)
  m_env_cfg.enable_scoreboard_checks = 1;
  char_seq.start(m_env.m_v_sqr.spi);
  phase.drop_objection(this, "Finished char_seq in main phase");
endtask: main_phase
