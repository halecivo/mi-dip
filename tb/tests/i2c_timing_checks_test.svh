class i2c_timing_checks_test extends sensor_test_base;

  `uvm_component_utils(i2c_timing_checks_test)

  extern function new(string name = "i2c_timing_checks_test", uvm_component parent = null);
  extern function void build_phase(uvm_phase phase);
  extern task reset_phase(uvm_phase phase);
  extern task main_phase(uvm_phase phase);

endclass: i2c_timing_checks_test

function i2c_timing_checks_test::new(string name = "i2c_timing_checks_test", uvm_component parent = null);
  super.new(name, parent);
endfunction

function void i2c_timing_checks_test::build_phase(uvm_phase phase);
  super.build_phase(phase);
endfunction: build_phase

task i2c_timing_checks_test::reset_phase(uvm_phase phase);
  //check_reset_seq reset_test_seq = check_reset_seq::type_id::create("reset_test_seq");
  m_i2c_cfg.Tscl_low     = 5.0us;
  m_i2c_cfg.Tscl_high    = 15.0us;
  m_i2c_cfg.Tmin_Setup_sda = 15.0us;
  m_i2c_cfg.Tmin_Hold_sda = 15.0us;

  phase.raise_objection(this, "Starting reset_test_seq in reset phase");
  //reset_test_seq.start(m_env.m_v_sqr.spi);
  #m_env_cfg.dut_setup_time;
  phase.drop_objection(this, "Finished reset_test_seq in reset phase");
endtask: reset_phase

task i2c_timing_checks_test::main_phase(uvm_phase phase);
  base_reg_seq     char_seq  = base_reg_seq::type_id::create("char_seq");

  phase.raise_objection(this, "Starting char_seq in main phase");
  char_seq.start(m_env.m_v_sqr.i2c);
  phase.drop_objection(this, "Finished char_seq in main phase");
endtask: main_phase
