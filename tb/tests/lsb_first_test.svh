class lsb_first_test extends sensor_test_base;

  `uvm_component_utils(lsb_first_test)

  extern function new(string name = "lsb_first_test", uvm_component parent = null);
  extern function void build_phase(uvm_phase phase);
  extern task reset_phase(uvm_phase phase);
  extern task main_phase(uvm_phase phase);

endclass: lsb_first_test

function lsb_first_test::new(string name = "lsb_first_test", uvm_component parent = null);
  super.new(name, parent);
endfunction

function void lsb_first_test::build_phase(uvm_phase phase);
  super.build_phase(phase);
endfunction: build_phase

task lsb_first_test::reset_phase(uvm_phase phase);
  //check_reset_seq reset_test_seq = check_reset_seq::type_id::create("reset_test_seq");

  phase.raise_objection(this, "Starting reset_test_seq in reset phase");
  //reset_test_seq.start(m_env.m_v_sqr.spi);
  #m_env_cfg.dut_setup_time;
  phase.drop_objection(this, "Finished reset_test_seq in reset phase");
endtask: reset_phase

task lsb_first_test::main_phase(uvm_phase phase);
  simple_reg_seq simple = simple_reg_seq::type_id::create("simple_reg_seq");
  m_spi_cfg.MSB = 0; 
  m_i2c_cfg.MSB = 0; 

  phase.raise_objection(this, "Starting simple_seq in main phase");
  // write LSB first
  simple.start(m_env.m_v_sqr.spi);
  simple = simple_reg_seq::type_id::create("simple_reg_seq");
  simple.write = 0;
  // read LSB first
  simple.start(m_env.m_v_sqr.spi);
  simple = simple_reg_seq::type_id::create("simple_reg_seq");
  simple.write = 0;
  m_spi_cfg.MSB = 1; 
  m_i2c_cfg.MSB = 1; 
  m_env_cfg.enable_scoreboard_checks = 0;
  m_env_cfg.enable_LSB_first_check = 1;
  // read MSB first, but reverse bits in scoreboard
  simple.start(m_env.m_v_sqr.spi);
  phase.drop_objection(this, "Finished simple_seq in main phase");

  #500us;
  $display(m_env.m_scoreboard.register_get(7'h07));
endtask: main_phase
