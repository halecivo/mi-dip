class multi_read_test extends sensor_test_base;

  `uvm_component_utils(multi_read_test)

  extern function new(string name = "multi_read_test", uvm_component parent = null);
  extern function void build_phase(uvm_phase phase);
  extern task reset_phase(uvm_phase phase);
  extern task main_phase(uvm_phase phase);

endclass: multi_read_test

function multi_read_test::new(string name = "multi_read_test", uvm_component parent = null);
  super.new(name, parent);
endfunction

function void multi_read_test::build_phase(uvm_phase phase);
  super.build_phase(phase);
endfunction: build_phase

task multi_read_test::reset_phase(uvm_phase phase);
  //check_reset_seq reset_test_seq = check_reset_seq::type_id::create("reset_test_seq");

  phase.raise_objection(this, "Starting reset_test_seq in reset phase");
  //reset_test_seq.start(m_env.m_v_sqr.spi);
  #m_env_cfg.dut_setup_time;
  phase.drop_objection(this, "Finished reset_test_seq in reset phase");
endtask: reset_phase

task multi_read_test::main_phase(uvm_phase phase);
  multi_reg_seq     multi_seq  = multi_reg_seq::type_id::create("multi_read_seq");

  phase.raise_objection(this, "Starting multi_read_seq in main phase");
  multi_seq.start(m_env.m_v_sqr.spi);
  phase.drop_objection(this, "Finished multi_read_seq in main phase");
endtask: main_phase
