class sensor_env_config extends uvm_object;

  localparam string s_my_config_id = "sensor_env_config";
  localparam string s_no_config_id = "no config";
  localparam string s_my_config_type_error_id = "config type error";

  `uvm_object_utils(sensor_env_config)

  // Data members
  bit has_scoreboard = 1;

  bit has_spi_agent = 1;
  bit has_adc_agent = 1;
  bit has_i2c_agent = 1;

  bit has_virtual_sequencer = 1;

  bit enable_scoreboard_checks = 1;
  bit enable_LSB_first_check = 0;

  time dut_setup_time = 1ms;

  spi_agent_config m_spi_agent_cfg;
  i2c_agent_config m_i2c_agent_cfg;
  adc_agent_config m_adc_agent_cfg;


  // Methods
  extern function new(string name = "sensor_env_config");

endclass: sensor_env_config


function sensor_env_config::new(string name = "sensor_env_config");
  super.new(name);
endfunction

