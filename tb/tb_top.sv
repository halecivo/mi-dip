
// Provide the clock and reset signals to the OR10 test bench
// when running under Icarus Verilog.
//
// Copyright (c) 2012, R. Diez

`include "timescale.sv"

`define NS_IN_SEC  1_000_000_000
`define HZ_IN_MHZ  1_000_000

`define FREQ_IN_MHZ 25  // This frequency does not matter much for simulation purposes.
`define FREQ_IN_HZ ( `FREQ_IN_MHZ * `HZ_IN_MHZ )
`define CLK_PERIOD ( `NS_IN_SEC / `FREQ_IN_HZ )


module tb_top;

   import uvm_pkg::*;
   import sensor_test_lib_pkg::*;

   reg clock, reset;
   wire sda, scl;

   wire dut_scl_oe, dut_scl_o, dut_sda_oe, dut_sda_o;

   spi_if SPI();
   i2c_if I2C();
   adc_if ADC();
   clk_if CLK();

   sensor_top sensor_instance ( 
            .wb_clk_i(CLK.clk),
            .wb_rst_i(CLK.reset),
            .spi_ssel_i(SPI.csb),
            .spi_sck_i(SPI.sck),
            .spi_mosi_i(SPI.mosi),
            .spi_miso_o(SPI.miso),
            .scl_in(scl),
            .scl_o(dut_scl_o),
            .scl_oe(dut_scl_oe),
            .sda_in(sda),
            .sda_o(dut_sda_o),
            .sda_oe(dut_sda_oe),
            .adc_start_o(ADC.start),
            .adc_ready_i(ADC.ready),
            .adc_data_i(ADC.data),
            .adc_clk_o(ADC.clk),
            .adc_en_o(ADC.en)
                                  );

   // I2C interconnection
   pullup(scl);
   pullup(sda);

   bufif0(scl, dut_scl_o, dut_scl_oe);
   bufif0(scl, 1'b0, I2C.scl_o);
   bufif0(sda, dut_sda_o, dut_sda_oe);
   bufif0(sda, 1'b0, I2C.sda_o);

   assign I2C.scl_i = scl;
   assign I2C.sda_i = sda;

   // UVM initial block
   initial begin
     uvm_config_db #(virtual clk_if)::set( null , "uvm_test_top" , "CLK_vif" , CLK);
     uvm_config_db #(virtual spi_if)::set( null , "uvm_test_top" , "SPI_vif" , SPI);
     uvm_config_db #(virtual i2c_if)::set( null , "uvm_test_top" , "I2C_vif" , I2C);
     uvm_config_db #(virtual adc_if)::set( null , "uvm_test_top" , "ADC_vif" , ADC);
     CLK.clk_period = `CLK_PERIOD * 1ns;
     CLK.do_reset(3);
     CLK.enable_clk();
     run_test();
     $finish;
   end

endmodule
