class spi_cfg_seq extends base_reg_seq;
  `uvm_object_utils(spi_cfg_seq)

   bit cphase = 0;
   bit cpol   = 0;
   int setup_reg_addr = 7;

  extern function new(string name = "spi_cfg_seq");
  extern task body;

endclass: spi_cfg_seq

function spi_cfg_seq::new(string name = "spi_cfg_seq");
  super.new(name);
endfunction

task spi_cfg_seq::body;
  bus_seq_item req;

  begin
    req = bus_seq_item::type_id::create("req");
    start_item(req);
    req.write = 1;
    req.addr = REG_BUS_SETUP;
    req.push_byte( {6'h0, cphase, cpol} );
    finish_item(req);
  end

endtask: body
