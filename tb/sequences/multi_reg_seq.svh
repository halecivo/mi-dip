class multi_reg_seq extends base_reg_seq;

  `uvm_object_utils(multi_reg_seq)

  import bus_txn_pkg::*;

  extern function new(string name = "multi_reg_seq");
  extern task body;

endclass: multi_reg_seq


function multi_reg_seq::new(string name = "multi_reg_seq");
  super.new(name);
endfunction

task multi_reg_seq::body;
  bus_seq_item req;

  begin
    req = bus_seq_item::type_id::create("bus_seq_item");
    start_item(req);
    assert(req.randomize() with {
      addr == REG_CONST;
      write == 1;
      data_length == 8;
    });
    finish_item(req);
    req = bus_seq_item::type_id::create("bus_seq_item");
    start_item(req);
    assert(req.randomize() with {
      addr == REG_FREE1; // REG_CONST + 1
      write == 1;
      data_length == 8;
    });
    finish_item(req);
    req = bus_seq_item::type_id::create("bus_seq_item");
    start_item(req);
    req.addr = REG_CONST;
    req.data_length = 16;
    req.write = 0;
    finish_item(req);
  end

endtask: body


