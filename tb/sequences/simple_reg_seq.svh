class simple_reg_seq extends base_reg_seq;

  `uvm_object_utils(simple_reg_seq)

  import bus_txn_pkg::*;

  bit write = 1;

  extern function new(string name = "simple_reg_seq");
  extern task body;

endclass: simple_reg_seq


function simple_reg_seq::new(string name = "simple_reg_seq");
  super.new(name);
endfunction

task simple_reg_seq::body;
  bus_seq_item req;

  begin
    req = bus_seq_item::type_id::create("bus_seq_item");
    start_item(req);
    req.addr = REG_FREE1;
    req.write = write;
    if (write)
      req.push_byte(8'b11001100);
    else
      req.data_length = 8;
    finish_item(req);
  end

endtask: body


