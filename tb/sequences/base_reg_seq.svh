class base_reg_seq extends uvm_sequence #(bus_seq_item);

  `uvm_object_utils(base_reg_seq)

  import bus_txn_pkg::*;

  extern function new(string name = "base_reg_seq");
  extern task body;
  int num_txn = 10;

endclass: base_reg_seq


function base_reg_seq::new(string name = "base_reg_seq");
  super.new(name);
endfunction

task base_reg_seq::body;
  bus_seq_item req;

  begin
    for (int i = 0; i < num_txn; i++) begin
      req = bus_seq_item::type_id::create("bus_seq_item");
      start_item(req);
      assert(req.randomize() with {
        addr != REG_BUS_SETUP;
        write == 1;
        data_length == 8;
      });
      finish_item(req);
      start_item(req);
      req.write = 0;
      finish_item(req);
    end
  end

endtask: body


