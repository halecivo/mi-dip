class bus_err_seq extends base_reg_seq;

  `uvm_object_utils(bus_err_seq)

  extern function new(string name = "bus_err_seq");
  extern task body;

endclass: bus_err_seq


function bus_err_seq::new(string name = "bus_err_seq");
  super.new(name);
endfunction

task bus_err_seq::body;
  bus_seq_item req;

  begin
    for (int i = 0; i < 5; i++) begin
      req = bus_seq_item::type_id::create("request");
      start_item(req);
      assert(req.randomize() with {
        addr != REG_BUS_SETUP;
      });
      finish_item(req);
    end
  end

endtask: body


