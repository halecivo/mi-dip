class sensor_test_base extends uvm_test;

  `uvm_component_utils(sensor_test_base)

  // data members

  // component members
  sensor_env m_env;
  sensor_env_config m_env_cfg;
  spi_agent_config m_spi_cfg;
  i2c_agent_config m_i2c_cfg;
  adc_agent_config m_adc_cfg;

  extern function new(string name = "sensor_test_base", uvm_component parent = null);
  extern function void build_phase(uvm_phase phase);

endclass: sensor_test_base

function sensor_test_base::new(string name = "sensor_test_base", uvm_component parent = null);
  super.new(name,parent);
endfunction

function void sensor_test_base::build_phase(uvm_phase phase);
  m_env_cfg = sensor_env_config::type_id::create("m_env_cfg");

  m_spi_cfg = spi_agent_config::type_id::create("m_spi_cfg");
  if ( !uvm_config_db #(virtual spi_if)::get(this, "", "SPI_vif", m_spi_cfg.SPI) ) 
    `uvm_error("SPI_IF LOAD:","could not load SPI interface");

  m_i2c_cfg = i2c_agent_config::type_id::create("m_i2c_cfg");
  if ( !uvm_config_db #(virtual i2c_if)::get(this, "", "I2C_vif", m_i2c_cfg.I2C) ) 
    `uvm_error("I2C_IF LOAD:","could not load SPI interface");

  m_adc_cfg = adc_agent_config::type_id::create("m_adc_cfg");
  if ( !uvm_config_db #(virtual adc_if)::get(this, "", "ADC_vif", m_adc_cfg.ADC) ) 
    `uvm_error("ADC_IF LOAD:","could not load ADC interface");

  m_env_cfg.m_spi_agent_cfg = m_spi_cfg;
  m_env_cfg.m_i2c_agent_cfg = m_i2c_cfg;
  m_env_cfg.m_adc_agent_cfg = m_adc_cfg;
  uvm_config_db #(sensor_env_config)::set(this, "*", "sensor_env_config", m_env_cfg);
  m_env = sensor_env::type_id::create("m_env", this);

endfunction: build_phase


