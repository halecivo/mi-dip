`include "timescale.sv"

interface clk_if;
  bit clk_enable = 0;
  time clk_period = 1ns;

   // Number of rising clock edges the reset signal will be asserted.
   // Set it to 0 in order to start the simulation without asserting the reset signal,
   // which is handy to simulate FPGA designs without user reset signal that optimise away
   // the whole reset logic.
  int reset_hold_time = 3;  

  logic clk;
  logic reset;

  initial begin
    clk = 0;
    reset = reset_hold_time > 0 ? 1 : 0;
  end

  always begin
    #(clk_period/2);
    if (clk_enable) begin  
      clk <= ~clk;
    end
  end

  task do_reset(int hold_time);
    reset_hold_time = hold_time;
    reset <= 1;
  endtask;

  task enable_clk();
    clk_enable = 1;
  endtask

  task disable_clk();
    clk_enable = 0;
  endtask

  always @(posedge clk) begin
        if ( reset_hold_time > 1 ) begin
           reset <= 1;
           reset_hold_time <= reset_hold_time - 1;
        end
        else begin
           reset <= 0;
        end
  end

endinterface;
