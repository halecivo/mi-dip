package sensor_env_pkg;
`include "uvm_macros.svh"

import uvm_pkg::*;
import bus_txn_pkg::*;
import spi_agent_pkg::*;
import i2c_agent_pkg::*;
import adc_agent_pkg::*;

`include "sensor_env_config.svh"
`include "sensor_virtual_sequencer.svh"
`include "sensor_scoreboard.svh"
`include "sensor_env.svh"

endpackage: sensor_env_pkg
