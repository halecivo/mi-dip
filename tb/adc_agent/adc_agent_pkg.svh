package adc_agent_pkg;

  import uvm_pkg::*;
  `include "uvm_macros.svh"

  `include "adc_seq_item.svh"
  `include "adc_agent_config.svh"
  `include "adc_agent.svh"

endpackage: adc_agent_pkg
  
