class adc_agent extends uvm_component;
  
  `uvm_component_utils(adc_agent)

  adc_agent_config m_cfg;
  virtual adc_if ADC;
  uvm_analysis_port #(adc_seq_item) ap;
  
  extern function new(string name = "adc_agent", uvm_component parent = null);
  extern function void build_phase (uvm_phase phase);
  extern function void connect_phase (uvm_phase phase);
  extern task run_phase(uvm_phase phase);

endclass: adc_agent

function adc_agent::new(string name = "adc_agent", uvm_component parent = null);
  super.new(name, parent);
endfunction

function void adc_agent::build_phase (uvm_phase phase);
    if ( !uvm_config_db #(adc_agent_config)::get(this,"","adc_agent_config",m_cfg) ) begin
      `uvm_fatal("build_phase", "unable to get adc_agent_config")
    end
    ap = new("ap", this);
endfunction

function void adc_agent::connect_phase (uvm_phase phase);
  ADC = m_cfg.ADC;
endfunction

task adc_agent::run_phase (uvm_phase phase);
  adc_seq_item item;
  adc_seq_item cloned_item;
  forever begin
    item = adc_seq_item::type_id::create("item");
    @(posedge ADC.clk);
    ADC.ready <= 'b0;
    while (ADC.start != 'b1) @(posedge ADC.clk);
    for (int i = 0; i < m_cfg.meas_delay; i++) @(posedge ADC.clk);
    assert(item.randomize());
    ADC.data <= item.data;
    ADC.ready <= 'b1;
    $cast(cloned_item, item.clone());
    ap.write(cloned_item);
  end
endtask
