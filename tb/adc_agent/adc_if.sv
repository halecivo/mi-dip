`include "timescale.sv"

interface adc_if;
  logic[7:0] data;
  logic start;
  logic en;
  logic clk;
  logic ready;
endinterface: adc_if
