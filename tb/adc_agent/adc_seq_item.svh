class adc_seq_item extends uvm_sequence_item;
  `uvm_object_utils(adc_seq_item)
  extern function void do_copy(uvm_object rhs);

  rand logic[7:0] data;

endclass


function void adc_seq_item::do_copy(uvm_object rhs);
  adc_seq_item rhs_;

  if(!$cast(rhs_, rhs)) begin
    `uvm_fatal("do_copy", "cast of rhs object failed")
  end
  super.do_copy(rhs);
  // Copy over data members:
  data = rhs_.data;

endfunction:do_copy
