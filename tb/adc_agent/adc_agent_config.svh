class adc_agent_config extends uvm_object;
  
  `uvm_object_utils(adc_agent_config)

  virtual adc_if ADC;
  int meas_delay = 50000; // number of clock pulses between adc_start and adc_ready

  localparam string s_my_config_id = "adc_agent_config";
  localparam string s_no_config_id = "no config";
  localparam string s_my_config_type_error_id = "config type error";

  extern static function adc_agent_config get_config(uvm_component c);
  extern function new(string name = "adc_agent_config");

endclass: adc_agent_config


function adc_agent_config::new(string name = "adc_agent_config");
  super.new(name);
endfunction

function adc_agent_config adc_agent_config::get_config(uvm_component c);
  adc_agent_config t;
  if(!uvm_config_db #(adc_agent_config)::get(c, "", s_my_config_id, t)) begin
  `uvm_fatal("CONFIG_LOAD", $sformatf("Cannot get() configuration %s from uvm_config_db. Have you set() it?", s_my_config_id))
end

  return t;
endfunction: get_config
